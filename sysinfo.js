console.info("Application sysinfo was started");

$(document).ready(function() {
    $("#link_hostname").text(window.location.hostname);
    const categoriesFilename = "../data/categories.json";
    const xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", categoriesFilename);
    xmlhttp.onload = function() {
        if (this.status == 200) {
            const categories = JSON.parse(xmlhttp.response);
            const parentElement = document.getElementById("sysinfo_categories");
            for (let category in categories) {
                let _category = categories[category];
                console.log("Adding category " + _category);
                let linkAction = function() {
                    loadCategoryItems(_category, undefined);
                };
                appendListItemLink(parentElement, "category_" + _category, _category, linkAction);
            }
            $("#category_loading").hide();
    
            const firstCategory = categories[0];
            console.log(`loadCategoryItems(${firstCategory})`);
            loadCategoryItems(firstCategory, function(firstItem) {
                console.log("Selecting first item");
                loadDataForItem(firstCategory, 0, firstItem);
            });
        }
        else {
            console.warn(`Could not load ${categoriesFilename}, got status ${this.status} ${this.statusText}`);
        }
    };
    xmlhttp.send();
});

function loadDataForItem(category, itemnr, item) {
    console.log(category + "/" + item + " was clicked");
    $("#sysinfo_header").text(category + " - " + item);
    $("#sysinfo_details").text("Retrieving data for " + item + "...");
    const itemFilename = "../data/" + category + "_" + itemnr + ".txt";
    const xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", itemFilename);
    xmlhttp.onload = function() {
        if (this.status == 200) {
            $("#sysinfo_details").text(xmlhttp.response);
        }
        else {
            console.warn(`Could not load data for item "${itemFilename}", got status ${this.status} ${this.statusText}`);
        }
    };
    xmlhttp.send();
}
  
function loadCategoryItems(category, onFinished) {
    console.log("Loading items for category " + category);
    const categoryFilename = "../data/" + category + "_items.json";
    const xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", categoryFilename);
    xmlhttp.onload = function() {
        if (this.status == 200) {
            const items = JSON.parse(xmlhttp.response);
            console.log("Loaded items " + items);

            $("#sysinfo_items").empty();
            const parentElement = document.getElementById("sysinfo_items");
            for (let item in items) {
                let _item = items[item];
                console.log("Add " + category + "/" + _item);
                let linkAction = function() { loadDataForItem(category, item, _item) };
                appendListItemLink(parentElement, "item_" + category + item, _item, linkAction);
            }

            if (typeof onFinished === "function") {
                onFinished(items[0]);
            }
        }
        else {
            console.warn(`Could not load category items "${categoryFilename}", got status ${this.status} ${this.statusText}`);
        }
    };
    xmlhttp.send();
}

function appendListItemLink(parentElement, id, linkName, linkAction) {
    let newLink = document.createElement("a");
    newLink.innerHTML = linkName;
    newLink.href = "#";
    newLink.onclick = linkAction;
    newLink.id = id;
    let listItem = document.createElement("li");
    listItem.append(newLink);
    parentElement.append(listItem);
}
