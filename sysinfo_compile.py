﻿import json
import subprocess

configfile = open('sysinfo.json').read()
config = json.loads(configfile)

datadir = config["settings"]["datadir"]

def writeToFile(filename, content):
    outputFile = open(datadir + filename, "w")
    outputFile.write(content)
    outputFile.close()

categoriesJson = "[ "
for category in config["pages"]:
    pagenr = 0
    categoriesJson += "\"" + category + "\", "
    itemsJson = "[ "
    for page in config["pages"][category]:
        print("Processing " + str(pagenr) + " " + category + "/" + page)
        itemsJson += "\"" + page + "\", "
        try:
            command = config["pages"][category][page]["command"]
            print("  Command: " + command)
            result = subprocess.run(command.split(), stdout=subprocess.PIPE)
            filename = category + "_" + str(pagenr) + ".txt"
            print("  filename: " + filename)
            writeToFile(filename, result.stdout.decode('utf-8'))
        except Exception as ex:
            print(ex)
        pagenr = pagenr + 1
    itemsJson = itemsJson[:-2] + " ]"
    writeToFile(category + "_items.json", itemsJson)

categoriesJson = categoriesJson[:-2] + " ]"
writeToFile("categories.json", categoriesJson)